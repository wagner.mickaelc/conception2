# Détails utiles
### Initialisation d'un dépôt git
cd <path>

git init

git remote add origin <repo> OR git remote set-url origin <repo>

git pull --set-upstream origin master

### Générateur de gitignore :
https://www.toptal.com/developers/gitignore

### Utilisation d'un environnement virtuel
python -m venv .venv

start .venv\Scripts\activate.bat

pip list --local

### Requirements.txt
pip freeze > requirements.txt

pip install -r requirements.txt

### Divers
PyPi : Dépôt de paquets python.